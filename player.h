#ifndef PLAYER_H
#define PLAYER_H

#include "battlecharacter.h"

class Player : public BattleCharacter
{
public:
    Player();
    virtual ~Player();

    virtual void move(BattleCharacterManager *battleCharacterManager);

    void displayMoveInfo();

private:
    void readInput();
    void attackOrDefend(BattleCharacterManager *battleCharacterManager);

    int mIntValue;
};

#endif // PLAYER_H
