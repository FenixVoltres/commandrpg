#ifndef CONSOLE_H
#define CONSOLE_H

#include "defines.h"

class Console
{
public:
    static void displayMessage(const String &message);
    static void displayMessageWithLineEnding(const String &message);
    static void displayInt(int intValue);
    static void displayLineEnding();

    static int readInt();
};

#endif // CONSOLE_H
