#ifndef BATTLECHARACTER_H
#define BATTLECHARACTER_H

#include "defines.h"

class BattleCharacterManager;

class BattleCharacter
{
public:
    BattleCharacter();
    virtual ~BattleCharacter();

    virtual bool isDefending() const
    { return mIsDefending; }

    virtual bool isAlive() const
    {
        bool isAlive = (mLifePoints > 0);
        return isAlive;
    }
    bool isDead() const
    { return !isAlive(); }

    virtual void defend();
    virtual void attack(BattleCharacter *enemy);
    virtual void hit(int damage);
    virtual void move(BattleCharacterManager *battleCharacterManager);

    virtual const String name() const = 0;

protected:
    int mLifePoints;
    int mAttack;
    int mDefend;
    bool mIsDefending;

private:
    void recalculateLifePoints(int damage);
    void displayLifePointsInfo(int damage);
};

#endif // BATTLECHARACTER_H
