#include "foemanager.h"

#include "foecreator.h"
#include "foe.h"

FoeManager::FoeManager()
    : mFoe(0)
{
}

FoeManager::~FoeManager()
{
    if (mFoe)
        delete mFoe;
}

void FoeManager::createCharacter()
{
    FoeCreator foeCreator;
    mFoe = foeCreator.create();
}

BattleCharacter* FoeManager::character()
{
    return mFoe;
}
