#ifndef GAME_H
#define GAME_H

class PlayerManager;
class TurnManager;
class FoeManager;

class Game
{
public:
    Game();
    ~Game();

    void start();
    void init();

private:
    bool isOver() const;

    PlayerManager *mPlayerManager;
    TurnManager *mTurnManager;
    FoeManager *mFoeManager;
};

#endif // GAME_H
