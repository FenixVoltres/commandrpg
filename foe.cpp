#include "foe.h"

#include "battlecharactermanager.h"
#include "globals.h"
#include "console.h"

Foe::Foe()
{
    mLifePoints = Globals::randomInt(10, 200);
    mAttack = Globals::randomInt(10, 200);
    mDefend = Globals::randomInt(10, 200);
}

Foe::~Foe()
{
}

void Foe::move(BattleCharacterManager *battleCharacterManager)
{
    if (isDead())
        return;

    BattleCharacter::move(battleCharacterManager);

    int random = Globals::randomInt(0, 1);
    bool shouldAttack = (random == 1);

    if (shouldAttack)
        attack(battleCharacterManager->character());
    else
        defend();
}
