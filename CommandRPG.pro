TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    console.cpp \
    player.cpp \
    game.cpp \
    turnmanager.cpp \
    playermanager.cpp \
    playercreator.cpp \
    elf.cpp \
    foe.cpp \
    dwarf.cpp \
    foemanager.cpp \
    foecreator.cpp \
    battlecharacter.cpp \
    globals.cpp

HEADERS += \
    console.h \
    defines.h \
    player.h \
    game.h \
    turnmanager.h \
    playermanager.h \
    playercreator.h \
    elf.h \
    foe.h \
    dwarf.h \
    foemanager.h \
    foecreator.h \
    battlecharacter.h \
    globals.h \
    battlecharactermanager.h

