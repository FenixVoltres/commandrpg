#ifndef FOECREATOR_H
#define FOECREATOR_H

class Foe;

class FoeCreator
{
public:
    FoeCreator();

    Foe *create();
};

#endif // FOECREATOR_H
