#include <iostream>
#include <sstream>
#include "console.h"

using namespace std;

void Console::displayMessage(const String &message)
{
    cout << message;
}

void Console::displayMessageWithLineEnding(const String &message)
{
    cout << message << "\n";
}

void Console::displayInt(int intValue)
{
    OutputStringStream outputStringStream;
    outputStringStream << intValue;

    Console::displayMessage(outputStringStream.str());
}

void Console::displayLineEnding()
{
    cout << "\n";
}

int Console::readInt()
{
    int intValue;
    cin >> intValue;
    return intValue;
}
