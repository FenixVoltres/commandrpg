#include "elf.h"

#include "console.h"
#include "foe.h"

Elf::Elf()
{
    mLifePoints = 80;
    mAttack = 100;
    mDefend = 100;
    Console::displayMessageWithLineEnding("I'm an Elf!");
}

Elf::~Elf()
{
}
