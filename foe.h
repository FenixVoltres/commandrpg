#ifndef FOE_H
#define FOE_H

#include "battlecharacter.h"

class Foe : public BattleCharacter
{
public:
    Foe();
    virtual ~Foe();

    virtual const String name() const
    { return "Foe"; }

    virtual void move(BattleCharacterManager *battleCharacterManager);
};

#endif // FOE_H
