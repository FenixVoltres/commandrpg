#ifndef ELF_H
#define ELF_H

#include "player.h"

class Elf : public Player
{
public:
    Elf();
    virtual ~Elf();

    virtual const String name() const
    { return "Elf"; }
};

#endif // ELF_H
