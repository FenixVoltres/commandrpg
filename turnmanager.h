#ifndef TURNMANAGER_H
#define TURNMANAGER_H

#define TURN_LIMIT 10

class PlayerManager;
class FoeManager;

class TurnManager
{
public:
    TurnManager();

    void nextTurn(PlayerManager *playerManager, FoeManager *foeManager);
    bool hasTheLastTurnPassed() const;

private:
    void displayTurnInfo();

    int mCurrentTurnNumber;
};

#endif // TURNMANAGER_H
